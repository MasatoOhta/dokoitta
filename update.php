<!doctype html>
<html lang="ja">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    
    <link rel="stylesheet" href="css/add.css">
    <link rel="stylesheet" href="css/top-link.css">
    <title>更新完了</title>
  </head>
  <body>
    <?php
    require_once('config.php');

    
    $ob_name = $_POST['ob_name'];
    $place = $_POST['place'];
    $event = $_POST['event'];
    $ob_vol = (int) $_POST['ob_vol'];
    try {
        if (empty($_POST['ob_id'])) throw new Exception('ID不正');
        $id = (int) $_POST['ob_id'];
        $dbh = new PDO(DSN, DB_USERNAME, DB_PASSWORD);
        $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        //更新
        $sql = "UPDATE objects SET ob_name = ?, ob_vol = ?, place = ?, event = ? WHERE ob_id = ?";
        $stmt = $dbh->prepare($sql);
        $stmt->bindValue(1, $ob_name, PDO::PARAM_STR);
        $stmt->bindValue(2, $ob_vol, PDO::PARAM_INT);
        $stmt->bindValue(3, $place, PDO::PARAM_STR);
        $stmt->bindValue(4, $event, PDO::PARAM_STR);
        $stmt->bindValue(5, $id, PDO::PARAM_INT);
        $stmt->execute();
        $dbh = null;
        
        
        ?>
        <h1 class="message">備品情報の更新が完了しました</h1>
        <div class="btnarea">
          <a class="btn top-link" href="index.html">トップページに戻る</a>
        </div>
        <?php
    } catch (Exception $e) {
        echo "エラーが発生しました" . "<br>";
        echo htmlspecialchars($e->getMessage(), ENT_QUOTES, 'UTF-8') . "<br>";
        die();
    }

    ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>