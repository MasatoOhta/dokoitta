<!doctype html>
<html lang="ja">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    
    <link rel="stylesheet" href="css/top-link.css">
    <link rel="stylesheet" href="css/add.css">

    <title>登録完了画面</title>
  </head>
  <body>
    <?php
    require_once('config.php');

    //変数に代入
    $ob_name = $_POST['ob_name'];
    $place = $_POST['place'];
    $event = $_POST['event'];
    $ob_vol = (int) $_POST['ob_vol'];
    
    try{
        $dbh = new PDO(DSN, DB_USERNAME, DB_PASSWORD);
        $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "INSERT INTO objects (ob_name, ob_vol, place, event) VALUES (?, ?, ?, ?)";
        $stmt = $dbh->prepare($sql);
        $stmt->bindValue(1, $ob_name, PDO::PARAM_STR);
        $stmt->bindValue(2, $ob_vol, PDO::PARAM_INT);
        $stmt->bindValue(3, $place, PDO::PARAM_STR);
        $stmt->bindValue(4, $event, PDO::PARAM_STR);
        $stmt->execute();
        $dbh = null;
        ?> 
        <h1 class="message">登録が完了しました！</h1>
        
        <div class="btnarea">
          <a class="btn form-link" href="form.html">登録画面に戻る</a>
        </div>
        
        <div class="btnarea">
          <a class="btn top-link" href="index.html">トップページに戻る</a>
        </div>
        <?php
    } catch (Exception $e) {
        echo "エラー発生: " . htmlspecialchars($e->getMessage(), ENT_QUOTES, 'UTF-8') . "<br>";
        die();
    }


    ?>
    
  </body>
</html>