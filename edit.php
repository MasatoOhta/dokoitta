<!doctype html>
<html lang="ja">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <link rel="stylesheet" href="css/form.css">
    <link rel="stylesheet" href="css/top-link.css">
    <title>備品情報の編集</title>
  </head>
  <body>
    <?php
    require_once('config.php');

    try {
        if (empty($_GET['id'])) throw new Exception('ID不正');
        $id = (int) $_GET['id'];
        $dbh = new PDO(DSN, DB_USERNAME, DB_PASSWORD);
        $dbh->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM objects WHERE ob_id = ?";
        $stmt = $dbh->prepare($sql);
        $stmt->bindValue(1, $id, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);
        $dbh = null;
    } catch (Exception $e) {
        echo "エラーが発生しました" . "<br>";
        echo htmlspecialchars($e->getMessage(), ENT_QUOTES, "UTF-8") . "<br>";
        die();
    }
    
    ?>

    <div class="container">
      <h1 class="title">備品情報編集</h1>
      <form action="update.php" method="POST" class="storage">
        <div class="form-group">
          <label>備品名</label>
          <input type="text" name="ob_name" class="form-control" value="<?php echo htmlspecialchars($result['ob_name'], ENT_QUOTES, 'UTF-8'); ?>">
          
        </div>
          <div class="form-group">
            <label class="vol">個数</label>
            <input type="number" min="1" max="9999" name="ob_vol" class="form-control" value="<?php echo htmlspecialchars($result['ob_vol'], ENT_QUOTES, 'UTF-8'); ?>">
          </div>
          <div class="form-group">
            <label>しまった場所 </label>
            <input type="text" name="place" class="form-control" value="<?php echo htmlspecialchars($result['place'], ENT_QUOTES, 'UTF-8'); ?>">
          </div>
          <div class="form-group">
            <label>使用行事</label>
            <input type="text" name="event" class="form-control" value="<?php echo htmlspecialchars($result['event'], ENT_QUOTES, 'UTF-8'); ?>">
          </div>
          <input type="hidden" name="ob_id" class="form-control" value="<?php echo htmlspecialchars($result['id'], ENT_QUOTES, 'UTF-8'); ?>">

        
          <input type="submit" value="送信" class="submit-btn">
  
      </form>
      <div class="btnarea">
        <a class="btn top-link" href="index.html">トップページに戻る</a>
      </div>
    </div>

    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
    </body>
</html>