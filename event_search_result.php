<!doctype html>
<html lang="ja">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">

    <link rel="stylesheet" href="css/search.css">
    <link rel="stylesheet" href="css/top-link.css">
    <title>備品名検索結果</title>
  </head>
  <body>
    <?php
    require_once('config.php');

    $event = $_POST['event'];
    try{
        $dbh = new PDO(DSN, DB_USERNAME, DB_PASSWORD);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        $sql = "SELECT * FROM objects WHERE event = '$event'";
        $stmt = $dbh->query($sql);
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    ?>
        <div class="container text-center">
            <h1 class="title">さがす</h1>
            <div class="row">
                <div class="col-md-5 name-search-form">
                    <p>備品名で探す</p>
                    <form action="name_search_result.php" method="POST" class="form-inline form-row align-items-center">
                        <div class="form-group">
                            <label for="ob_name">備品名</label>
                                <input type="text" name="ob_name" class="form-control text" required>
                            <input type="submit" value="検索">
                        </div>
                    </form>
                </div>
                <div class="col-md-2"></div>
                <div class="col-md-5 event-search-form">
                    <p>行事名で探す</p>
                    <form action="event_search_result.php" method="POST" class="form-inline form-row align-items-center">
                        <div class="form-group">
                        <label for="ob_name">行事名</label>
                            <input type="text" name="event" class="form-control text" required>
                            <input type="submit" value="検索">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <table class="table">
            <tr>
                <th>備品名</th><th>個数</th><th>収納場所</th><th>使用行事</th><th>変更</th><th>削除</th>
            </tr>
            <?php foreach ($result as $row) : ?>
            <tr>
                <td><?php echo htmlspecialchars($row['ob_name'],ENT_QUOTES,'UTF-8')?></td>
                <td><?php echo htmlspecialchars($row['ob_vol'],ENT_QUOTES,'UTF-8')?></td>
                <td><?php echo htmlspecialchars($row['place'],ENT_QUOTES,'UTF-8')?></td>
                <td><?php echo htmlspecialchars($row['event'],ENT_QUOTES,'UTF-8')?></td>
                <td><?php echo "<a class='btn btn-primary' href=edit.php?id=" . htmlspecialchars($row['ob_id'], ENT_QUOTES, 'UTF-8') . ">変更</a>"; ?></td>
                <!-- <td><?php echo "<a href=delete.php?id=" . htmlspecialchars($row['ob_id'], ENT_QUOTES, 'UTF-8') . ">削除</a>"; ?></td> -->
                <td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModalCenter">削除</button></td>
            </tr>
            <!-- Modal -->
            <div class="modal fade" id="deleteModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="deleteModalCenterTitle">削除しますか？</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>削除すると、データは完全に消去されます。</p>
                            <p>削除してもよろしいですか？</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">キャンセル</button>
                            <a type="button" class="btn btn-primary" href=<?php echo "delete.php?id=" . htmlspecialchars($row['ob_id'], ENT_QUOTES, 'UTF-8'); ?> >削除する</a>
                        </div>
                    </div>
                </div>
            </div>
            <?php endforeach; ?>
        </table>

        <div class="btnarea">
            <a class="btn top-link" href="index.html">トップページに戻る</a>
        </div>


        <?php
        $dbh = null;
    } catch (Exception $e) {
        echo "エラー発生" . "<br>";
        //エラー内容確認
        // echo htmlspecialchars($e->getMessage(), ENT_QUOTES, 'UTF-8') . "<br>";
        die();
    }

    ?>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
  </body>
</html>