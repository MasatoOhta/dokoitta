# Dokoitta

「どこにしまったっけ？」を無くすための倉庫管理アプリです。

# Description

保育園での使用を目的としています。
使用する行事を登録しておくことで、行事ごとに備品の収納場所を検索することができます。

# Requirement

* MAMP 6.2
* PHP 7.3.21
* MySQL 5.7.30

# Install

MAMP
https://www.mamp.info/en/mac/

# Note

## テーブルの作成
```
CREATE TABLE `objects` (
  `ob_id` int(11) NOT NULL,
  `ob_name` varchar(45) NOT NULL,
  `ob_vol` int(9) NOT NULL,
  `place` varchar(45) NOT NULL,
  `event` varchar(45) NOT NULL,
  `update_date` datetime DEFAULT CURRENT_TIMESTAMP,
  `delete_date` datetime DEFAULT NULL,
  `delete_flag` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE `objects`
  ADD PRIMARY KEY (`ob_id`);

ALTER TABLE `objects`
  MODIFY `ob_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

```

## config.phpについて

```
define('DSN', ' DB名 ');
define('DB_USERNAME', ' ユーザーネーム ');
define('DB_PASSWORD', ' パスワード ');
```
DB名・ユーザーネーム・パスワードに、ご自身の情報を入力してください。

# Author

* 太田雅人
* E-mail:masato.ohta2210@gmail.com